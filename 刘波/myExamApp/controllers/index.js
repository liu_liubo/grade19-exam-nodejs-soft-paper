"use strict";

let fs = require("fs");
let path = require("path");
let router = require("koa-router")();

//过滤路径下控制器文件
function getFlies(url){
    let files = fs.readdirSync(url);

    let outFiles = files.filter(x=>{
        return x!=="index.js"
    });
    
    return outFiles;
}

//过滤控制器后的文件进行路由
function getRoute(files){
    files.forEach(element => {
        let tmppath = path.join(__dirname,element);
        let tmpname = require(tmppath);
        for(let i in tmpname){
            let type = tmpname[i][0];
            let fn = tmpname[i][1];
            if(type==="get"){
                router.get(i,fn);
            }else if(type==="post"){
                router.post(i,fn);
            }
        }
    });
}


module.exports=function(){
    let files = getFlies(__dirname);
    getRoute(files);
    return router.routes();
}