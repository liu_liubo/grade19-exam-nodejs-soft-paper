"use strict";

let koa = require("koa");
let bodyparser = require("koa-bodyparser");
let css = require("koa-static");
let temp = require("./templating");
let controllers = require("./controllers/index")

let app = new koa();

//静态模块
app.use(css(__dirname));
//处理post请求
app.use(bodyparser());
//模板引擎
app.use(temp);
//路由
app.use(controllers());
//监听端口
app.listen(3000);
console.log("http://127.0.0.1:3000");